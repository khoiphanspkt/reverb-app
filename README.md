<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Exploring Laravel Reverb

[Source: ](https://laracasts.com/series/lukes-larabits/episodes/17)

## To start Project

# Copy file .env.example to .env
    cp .env.example .env

# Install Composer
    composer install

# Install npm
    npm install

# Run migrate and seeder:
    php artisan migrate:refresh --seed

# Start Laravel Reverb Server:
    php artisan reverb:start --host="0.0.0.0" --port=9999 (custom port in .env file)

# Start Laravel localhost:
    php artisan serve

# Watch queue list listen:
    php artisan queue:listen

# Start node to build FE:
    npm run dev
